import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid,loginType,telphone,telphoneCode) {
  const data = {
    username,
    password,
    code,
    uuid,
    loginType,
    telphone,
    telphoneCode
  }
  return request({
    url: '/login',
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    method: 'get'
  })
}

// 获取手机验证码
export function getSmsCode(data) {
  return request({
    url: '/getSmsCode',
    method: 'post',
    data: data
  })
}
// 通过手机号验证用户是否注册
export function getBindTel(data) {
  return request({
    url: '/getBindTel',
    method: 'post',
    data: data
  })
}