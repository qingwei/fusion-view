# 二维码

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 内容：扫描二维码显示的内容地址，例如设为“https://www.baidu.com/”，如下图；

  <viewer>
  <img src="../image/qrcode-1.png"width="60%">
  </viewer>

- 尺寸：二维码宽高大小，例如设置为 400，如下图；

  <viewer>
  <img src="../image/qrcode-2.png"width="60%">
  </viewer>

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
