var LampOption = {
};

LampOption.init = function(div,loopDelay) {
	//初始化构造器
	LampOption.initMq(div,loopDelay);
	//启动跑马灯
	$($("#"+div).children(0))[0].start();
	
}
LampOption.initMq = function(div,speed){
    var obj = $($("#"+div).children(0))[0];
	var objs;
    //定义跑马灯对象的自定义属性
    obj.currentItem = -1;
    obj.loopDelay = speed;
    obj.loopItems = new Array();
    obj.loopTimer = null;
    obj.speedX = 2;//向左移动距离
    obj.speedY = 2;//向上移动距离

    //定义跑马灯对象的自定义方法
    obj.loop = LampOption.loop;
    obj.start = LampOption.startLoop;
    obj.stop = LampOption.stopLoop;
    obj.bindingDiv = div;
    //定义跑马灯对象的事件
    obj.onmouseover = function(){ this.stop(); }
    obj.onmouseout = function(){ this.loop(); }
    //获取跑马灯对象的所有子元素
    objs = obj.getElementsByTagName("div");
    for(var i=0; i<objs.length; i++){
        //在loopItems属性中记录子元素
        obj.loopItems.push(objs[i]);
        //自定义子元素的属性和方法
        objs[i].index = i;
        objs[i].move = LampOption.move;
        objs[i].reset = LampOption.reset;
        //初始化子元素的状态
        objs[i].reset();
    }
}
//设置跑马灯移动后位置
LampOption.move = function(x, y){
    this.style.left = x + "px";
    this.style.top = y + "px";
}
//定义循环事件
LampOption.loop = function(){
	var obj;
    clearTimeout(this.loopTimer);
    //设置队列中循环展示项
    if(this.currentItem >= this.loopItems.length){
    	this.currentItem = 0;
    }
    //获取执行的跑马灯
    obj = this.loopItems[this.currentItem];
    //执行移动
    if((obj.offsetLeft+obj.offsetWidth)!=this.offsetLeft && obj.offsetLeft+obj.offsetWidth>=0){
    	//向左卷动
        obj.move(obj.offsetLeft - this.speedX, obj.offsetTop);
    }/* else if(obj.offsetTop + obj.offsetHeight > this.offsetTop){
        //向上卷动
        obj.move(obj.offsetLeft, obj.offsetTop - this.speedX);
    } */else{
        //重置该子元素
        obj.reset();
        this.currentItem++;
    }
    this.loopTimer = setTimeout("$($(\"#"+this.bindingDiv+"\").children(0))[0].loop();", this.loopDelay);
}
//重置，返回队列起始位置
LampOption.reset = function(obj){
	var p = this.parentNode;
    this.move(p.offsetLeft + p.offsetWidth, p.offsetTop);
}
//开始执行  
LampOption.startLoop = function(){
	for(var i=0; i<this.loopItems.length; i++){
		this.loopItems[i].reset();
	}
    this.currentItem = 0;
    this.loop();
    
}
//暂停执行
LampOption.stopLoop = function(){
    clearTimeout(this.loopTimer);
}
