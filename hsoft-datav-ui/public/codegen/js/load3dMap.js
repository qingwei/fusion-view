function load3DMap(mapCode, name, myChart, dataOption) {
	//console.log("QAQ", dataOption);

      $.get(mapCode, function(geoJson) {
          if (geoJson) {
            echarts.registerMap(name, geoJson);

            const data = geoJson.features.map((item, index) => {
                const geoAreaName = item.properties.name; // geo文件中的地理名称
                
                return {
                  name: geoAreaName,
                
                  coord: item.properties.centroid,
                  selected: true
                  // x: 150,
                  // y: 150
                }
            })
          


        var option = {
          // title: { // 标题
          //   top: '2%',
          //   text: '3D地图',
          //   subtext: '',
          //   x: 'center',
          //   textStyle: {
          //     color: '#ccc'
          //   }
          // },
          selectedMode : 'multiple', // 选中效果固话
          tooltip: { // 提示框
            show: true,
            trigger: 'item',
            formatter: function (params) {
            return params.name;
            }
          },
          series: [{
            name: 'map3D',
            type: "map3D", // map3D / map
            map: name,
            label: { // 标签的相关设置
            show: true, // (地图上的城市名称)是否显示标签 [ default: false ]
            // distance: 5, // 标签距离图形的距离，在三维的散点图中这个距离是屏幕空间的像素值，其它图中这个距离是相对的三维距离
            //formatter:, // 标签内容格式器
            textStyle: dataOption.series[0].label.textStyle ,// 标签的字体样式
            // normal:{
            //   show:true,
            //   formatter:function(params){ //标签内容
            //     // console.log(params)
            //     return  params.name;
            //   },
            //   // lineHeight: 20,
            //   backgroundColor:'rgba(255,255,255,.9)',
            //   borderColor:'#80cffd',
            //   borderWidth:'1',
            //   padding:[5,15,4],
            //   color:'#000000',
            //   fontSize: 12,
            //   fontWeight:'normal',
            // },
            emphasis: {
              show: true
            }
            },
            tooltip: { //提示框组件。
            alwaysShowContent:true,
            hoverAnimation: true,
            trigger: 'item', //触发类型 散点图
            enterable: true, //鼠标是否可进入提示框
            transitionDuration: 1, //提示框移动动画过渡时间
            triggerOn: 'click',
            formatter: function (params) {
              // console.log(params.name, 'params.name')
              const tooltip = dataOption.staticDataValue.filter(function(item){
              
              return item.name == params.name;
              })
              if (tooltip[0]) {
              var str = `
                <div class="map-tooltip">
                <div class="city-name">${tooltip[0].name}</div>`;
                tooltip[0].value.forEach(element => {
                  str += `<div class="city-info">${element}</div>`
                });
                str = str + `</div>`;
                
              
              return str;
              }
            },
            backgroundColor: dataOption.series[0].tooltip.backgroundColor,
            borderWidth: dataOption.series[0].tooltip.borderWidth,
            borderRadius: dataOption.series[0].tooltip.borderRadius,
            borderColor: dataOption.series[0].tooltip.borderColor,
            textStyle: {
              color: dataOption.series[0].tooltip.textStyle.color
            },
            padding: [5, 10]
            },
            itemStyle: dataOption.series[0].itemStyle,


            data: data,
            // 3d地图添加 markPoint 位置不对
            /*markPoint:{
            symbolSize: 45,
            symbol: 'path://m 0,0 h 48 v 20 h -30 l -6,10 l -6,-10 h -6 z',
            itemStyle: {
              normal: {
              borderColor: '#33CBFF',
              color:'#33CBFF',
              borderWidth: 1,            // 标注边线线宽，单位px，默认为1
              label: {
                show: true
              }
              }
            },
            data: data
            }*/
          }],
        };
            myChart.setOption(option);
        } else {
            alert('无法加载该地图');
        }
    });
}